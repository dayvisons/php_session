<?php

    session_start();

    if ($_SESSION["login"] == 1) {
        // echo "Validação feita com sucesso. \n Seja bem vindo.";
        unset($_SESSION["login"]);
        unset($_SESSION["error"]);
    } else {
        $_SESSION["error"] = 2;
        header("location: projeto2.php");
    }

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Estilo CSS -->
    <link rel="stylesheet" type="text/css" href="css/estilo.css">

    <title>Bem vindo ao Painel de Controle</title>
</head>
<body>
    <div class="containerGato">
        <img src="imagens/imagem2.jpg">
        <br><br>
        <a href="http://localhost/portifolio/atividadepratica2/projeto2.php">Voltar</a>
    </div>
</body>
</html>