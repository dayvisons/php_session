<?php 
    session_start();
    //$sessionUser = $_POST['user'];
    //$sessionPassword = $_POST['password'];
    //$user = $_SESSION["sessionUser"];
    //password = $_SESSION["sessionPassword"];

    //echo "Usuário: ".$user;
    //echo "<br>Senha: ".$password;
    
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Redefina sua senha</title>
    <!-- Estilo CSS -->
    <link rel="stylesheet" type="text/css" href="css/estilo.css">
</head>
<body>
    <div id="containerRedefine">
            <form method="post" action="projeto2.php">
                Usuário Atual:  <?php echo $_SESSION["sessionUser"]; ?><br> 
                Senha Atual: <?php echo $_SESSION["sessionPassword"]; ?><br> 
                <br>
                <div>
                    Usuário: <input type="text" name="user"  id="user" placeholder="Digite seu usuário">
                </div>
                <div>
                    Senha: <input type="password" name="password" id="password" placeholder="Digite sua senha">
                </div>
                <div>
                    Redigite a senha: <input type="password" name="passwordConf" id="passwordConf" placeholder="Digite novamente a sua senha">
                </div>
                <div>
                    <button type="submit" id="Enviar" onclick="passwordValidation()">Enviar</button>
                </div>
                <div>
                    <button onclick="window.location.href='projeto2.php'" class="btn waves-effect waves-light btn-flat">Voltar</button>
                </div>
            </form>
        </div>    
    <script>
    var password = document.getElementById("password");
    var passwordConf = document.getElementById("passwordConf");

    function passwordValidation() {
        if (password != passwordConf) {
            alert("Senhas não coincidem!");
        }
    }
    
    </script>

</body>
</html>