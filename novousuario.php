<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Atividade 2</title>
    
    <!-- Estilo CSS -->
    <link rel="stylesheet" type="text/css" href="css/estilo.css">
</head>
<body>

<div id="container">
        <form method="post" action="projeto2.php" onsubmit="event.preventDefault(); passwordValidation();">
            <div>
                Usuário: <input type="text" name="user"  id="user" placeholder="Digite seu usuário">
            </div>
            <div>
                Senha: <input type="password" name="password" id="password" placeholder="Digite sua senha">
            </div>
            <div>
                Redigite a senha: <input type="password" name="passwordConf" id="passwordConf" placeholder="Digite novamente a sua senha" onblur="verifica()">
            </div>
            <div>
                <button type="submit" id="Enviar" onclick="window.location.href='projeto2.php'">Enviar</button>
            </div>
        </form>
    </div>
    
    <script>
        function passwordValidation() {
            var password = document.getElementById("password").value;
            var passwordConf = document.getElementById("passwordConf").value;

            if (password == passwordConf) {
                alert("Usuário criado com sucesso!");
                document.getElementById("newUserForm").submit();
                
            } else {
                alert("Senhas não coincidem!")
                return false;
            }    
        }
    </script>
</body>
</html>
