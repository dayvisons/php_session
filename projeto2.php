<?php

    session_start();
    if (isset($_POST["passwordConf"])) {
        $_SESSION["sessionUser"] = $_POST["user"];
        $_SESSION["sessionPassword"] = $_POST["passwordConf"];
    }

    if (isset($_SESSION["error"])) {
        if ($_SESSION["error"] == 1) {
        echo "Senha inválida!";
    } elseif ($_SESSION["error"] == 2) {
        echo "Efetuar login antes de entrar!";
    } else {
        echo "Algum erro ocorreu!";
    }
    }

?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Atividade 2</title>
    <!-- Estilo CSS -->
    <link rel="stylesheet" type="text/css" href="css/estilo.css">
</head>
<body>

    <div id="container">
            <form method="post" action="validacao.php">
                <div>
                   <b>Usuário: </b><br><input type="text" name="user" placeholder="Digite seu nome" id="user" /><br/><p></p>
                </div>
                <div>
                   <b>Senha: </b><br><input type="password" name="password" placeholder="Digite sua senha" id="password" /><br/>
                </div>
                <div>
                    <button type="submit" value="enviar" name="Enviar">Entrar</button>
                </div>
            </form>
        
            <!-- Botões -->
            <div>
                <div class="col">
                    <div>
                        <a href="novousuario.php">Novo Usuário</a>
                    </div>
                 </div><br>
                <div class="col">
                    <div>
                        <a href="redefinesenha.php">Esqueceu sua senha?</a>
                     </div>
                </div>
            </div>
        </div>

</body>
</html>